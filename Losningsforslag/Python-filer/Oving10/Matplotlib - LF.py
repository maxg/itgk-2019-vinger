#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from matplotlib import pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import math

print("Oppgave a:\n")
x_verdier = []
for i in range(0, 301):
    x_verdier.append(i/10)
print(x_verdier)

print("\nOppgave b:\n")
y_verdier = []
for x in x_verdier:
    y_verdier.append(math.sin(x))
print (y_verdier)
# Kunne også skrevet: y_verdier = [math.sin(x) for x in x_verdier]

print("\nOppgave c:\n")
plt.plot(x_verdier, y_verdier, 'r-')
plt.show()

